import { Component } from '@angular/core';

@Component({
  selector: 'onet-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'onet-mobile';
}
